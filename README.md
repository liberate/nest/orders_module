# Nest Orders Module

Handles ordering and fulfillment, including:

* Orders and Invoices
* Product Catalog
* Shipping Labels

## License

Copyright (c) 2024 Nest Developers

GNU Affero General Public License, Version 3 or later. https://www.gnu.org/licenses/agpl-3.0.en.html
