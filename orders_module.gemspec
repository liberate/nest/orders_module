Gem::Specification.new do |spec|
  spec.name        = "orders_module"
  spec.version     = "0.1.0"
  spec.authors     = ["Nest Developers"]
  spec.summary     = "Orders, invoices, products for Nest"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end
end
