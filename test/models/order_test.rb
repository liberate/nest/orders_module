require_relative '../test_helper'

class OrderTest < ActiveSupport::TestCase

  def test_creator
    # three ways to create an order

    order1 = Order.creator do |order|
      order.add_line(title: 'internet', amount: 25, type: 'internet_service')
      order.add_line(title: 'device', amount: 100, type: 'hotspot_device')
    end

    order2 = Order.creator(
      lines: [
        {title: 'internet', amount: 25, type: 'internet_service'},
        {title: 'device', amount: 100, type: 'hotspot_device'}
      ]
    )

    order3 = Order.creator(
      lines: {
        "1" => {title: 'internet', amount: 25, type: 'internet_service'},
        "2" => {title: 'device', amount: 100, type: 'hotspot_device'}
      }
    )

    [order1, order2, order3].each do |order|
      assert order.persisted?
      assert_equal 2, order.order_lines.reload.count
      assert_equal 'internet', order.order_lines.first.title
      assert_equal 'device', order.order_lines.last.title
      assert_equal Money.us_dollar(125_00), order.amount
      assert_equal 125_00, order.total_cents
    end
  end

  def test_create_by_product_type
    product_type = ProductType.create(title: 'western red cedar')
    assert product_type&.persisted?
    order = Order.creator do |order|
      order.add_line product_type: product_type
    end
    assert order&.persisted?
    assert order.errors.empty?, order.errors.full_messages
    assert_equal "western red cedar", order.order_lines.first.title
  end

  def test_destroy
    order = Order.create
    assert order.persisted?
    assert_nothing_raised do
      order.destroy!
    end
  end

end