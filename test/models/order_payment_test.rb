require_relative '../test_helper'

class OrderPaymentTest < ActiveSupport::TestCase
  MODE = :none # :all, :none, :new_episodes, :once

  def setup
    Stripe.api_key = "DUMMY_KEY" if MODE == :none

    VCR.configure do |c|
      c.cassette_library_dir = Orders::Engine.root.join('test', 'network')
      c.hook_into :webmock
      c.allow_http_connections_when_no_cassette = true
      c.define_cassette_placeholder("<STRIPE_PRIVATE_KEY>") { ENV['STRIPE_PRIVATE_KEY'] }
    end
  end

  def test_charge_success
    VCR.use_cassette('stripe-success', record: MODE) do
      member = Member.create!(email: 'salal@example.org')
      member.set_payment_processor :stripe
      member.payment_processor.update_payment_method "pm_card_visa"
      order = Order.create!(total_cents: 100_00, member: member)
      order.finalize_payment(member.payment_processor)
      assert order.is_paid?
      assert_equal 100_00, member.charges&.first&.amount_captured
      assert_equal -100_00, member.transactions.last.amount_cents
    end
  end

  def test_charge_rejected
    VCR.use_cassette('stripe-rejected', record: MODE) do
      member = Member.create!(email: 'salal@example.org')
      member.set_payment_processor :stripe
      member.payment_processor.update_payment_method "pm_card_chargeCustomerFail"
      order = Order.create!(total_cents: 200_00, member: member)
      order.finalize_payment(member.payment_processor)
      assert order.state_error?
      assert !order.is_paid?
      assert member.charges.empty?
      transaction = member.transactions.last
      assert transaction
      assert_equal "card_declined", transaction.error["code"]
    end
  end
end
