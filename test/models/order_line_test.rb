require_relative '../test_helper'

class OrderLineTest < ActiveSupport::TestCase

  def test_creator
    order = Order.create!
    line = OrderLine::HotspotDevice.creator(order: order, amount: 100, title: 'Hotspot')
    assert line.persisted?
    assert line.is_a?(OrderLine::HotspotDevice)
    assert_equal Money.us_dollar(100_00), line.amount

    line = OrderLine.creator(order: order, amount_cents: 100_00, title: 'Hotspot', type: 'hotspot_device')
    assert line.persisted?
    assert_equal Money.us_dollar(100_00), line.amount
    assert line.is_a?(OrderLine::HotspotDevice)
  end

  def test_creator_by_product_type
    product_type = ProductType.create(title: 'western red cedar', order_line_type: 'internet_service')
    assert product_type&.persisted?
    order = Order.create!
    line = OrderLine.creator(order: order, amount_cents: 100_00, product_type: product_type)
    assert line.persisted?, line.errors.full_messages
    assert line.is_a?(OrderLine::InternetService)
  end
end