Orders::Engine.routes.draw do
  resources :orders, path: '/'
  get '/item/details/:id', to: 'details#show', as: 'product_details'
end
