Admin::Orders::Engine.routes.draw do
  resources :product_types
  resources :order_lines
  resources :shipping_labels
  resources :shipments
  resources :orders, path: '/'
end