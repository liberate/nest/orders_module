module Orders
  class Engine < Rails::Engine
    include Nest::UserEngine
    engine_name 'orders'
    isolate_namespace Orders
  end
end

module Admin
  module Orders
    class Engine < Rails::Engine
      include Nest::AdminEngine
      engine_name 'admin_orders'
      isolate_namespace Admin::Orders
    end
  end
end
