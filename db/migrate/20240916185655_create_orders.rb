class CreateOrders < ActiveRecord::Migration[7.1]
  def change
    create_table "orders", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.timestamps

      t.string "code"
      t.string "type"
      t.date "date"

      t.integer "shipment_id"
      t.integer "member_id"

      t.integer "amount_cents"
      t.integer "value_cents"
      t.integer "tax_cents"
      t.integer "total_cents"
      t.string "currency", limit: 4, default: "USD"

      t.integer "state"
      t.integer "is_assignment_required", default: false
      t.integer "has_assignment", default: false

      t.boolean "is_fulfilled", default: false
      t.boolean "is_shipping_required", default: false
      t.boolean "has_sent_received", default: false
      t.boolean "has_sent_fulfilled", default: false
      t.boolean "has_sent_tracking", default: false

      t.text "data"

      t.index ["code"], name: "index_orders_on_code"
    end

    create_table "order_lines", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "order_id"
      t.integer "product_id"
      t.integer "product_type_id"

      t.string "type"
      t.integer "position"

      t.datetime "created_at"
      t.datetime "period_start_at"
      t.datetime "period_end_at"

      t.string "title"
      t.string "subtitle"

      t.integer "quantity", default: 1
      t.integer "quantity_type", default: 0

      t.integer "unit_amount_cents"
      t.integer "unit_value_cents"
      t.integer "amount_cents", default: 0
      t.integer "value_cents", default: 0
      t.boolean "is_taxable", default: false
      t.integer "tax_id"
      t.integer "tax_cents"
      t.string "currency", limit: 4, default: "USD"

      t.text "data"

      t.index ["order_id"], name: "index_order_lines_on_order_id"
    end

    create_table "product_types", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.timestamps

      t.string "type"
      t.string "order_line_type"
      t.string "code"
      t.string "title"
      t.string "subtitle"
      t.string "description"

      t.string "category"
      t.string "region"

      t.boolean "is_taxable", default: false
      t.integer "tax_profile_id", default: nil
      t.integer "quantity_type", default: 0
      t.integer "price_cents", default: 0
      t.integer "value_cents", default: 0
      t.string "currency", limit: 4, default: "USD"

      t.boolean "is_service", default: false
      t.boolean "is_active", default: false
      t.boolean "is_visible", default: false
      t.integer "inventory", default: 0
      t.integer "warranty_id"

      t.boolean "is_shipping_required", default: false
      t.boolean "is_assignment_required", default: false
      t.string "shippable_to"
      t.string "distance_unit", default: "cm"
      t.float "height"
      t.float "width"
      t.float "depth"
      t.string "mass_unit", default: "kg"
      t.float "weight"

      t.text "data"
    end

    create_table "product_type_services", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer :product_type_id
      t.integer :service_id
    end

    create_table "products", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.timestamps

      t.string "type"
      t.integer "member_id"
      t.integer "product_type_id"

      t.string "identifier", limit: 191
      t.string "code", limit: 191

      t.boolean "is_active", default: true
      t.date "warranty_start_on"
      t.date "warranty_end_on"
      t.datetime "end_at"
      t.integer "replacement_for_id"

      t.integer "placement"
      t.text "data"

      t.index ["identifier"], name: "index_products_on_identifier", unique: true
      t.index ["member_id"], name: "index_products_on_member_id"
    end

    create_table "service_periods", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "product_id"
      t.integer "order_line_id"
      t.boolean "is_paid"
      t.datetime "start_at"
      t.datetime "end_at"
    end

    create_table "shipments", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.timestamps
      t.boolean "is_shipped", default: false
      t.boolean "is_closed", default: false
      t.datetime "shipped_at"
      t.integer "admin_id"
    end

    create_table "shipping_labels", charset: "utf8mb4", collation: "utf8mb4_general_ci", force: :cascade do |t|
      t.timestamps
      t.integer "order_id"
      t.boolean "is_valid", default: false
      t.boolean "is_paid", default: false
      t.boolean "has_error", default: false
      t.string "provider"
      t.integer "amount_cents"
      t.string "currency", limit: 4, default: "USD"
      t.string "resource_id"
      t.string "tracking_id"
      t.string "rate_id"
      t.text "shipment"
      t.text "purchase"
      t.index ["order_id"], name: "index_shipping_labels_on_order_id"
      t.index ["tracking_id"], name: "index_shipping_labels_on_tracking_id"
    end
  end
end