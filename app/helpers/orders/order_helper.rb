module Orders::OrderHelper

  protected

  ORDER_STATE_BADGES = {
    "new"      => ["primary", "new"],
    "paid"     => ["success", "paid"],
    "pending"  => ["warning", "pending"],
    "error"    => ["danger", "error"],
    "canceled" => ["secondary", "canceled"],
    "refunded" => ["dark", "refunded"]
  }

  def order_badges(order)
    badges = []
    badge_state_class = "text-bg-" + ORDER_STATE_BADGES[order.state][0]
    badge_state_label = ORDER_STATE_BADGES[order.state][1]
    badges << content_tag(:span, class: 'badge rounded-pill ' + badge_state_class) { badge_state_label }
    if order.is_shipping_required?
      if order.is_fulfilled?
        badges << content_tag(:span, class: 'badge rounded-pill text-bg-success') { "shipped" }
      else
        badges << content_tag(:span, class: 'badge rounded-pill text-bg-secondary') { "not shipped" }
      end
    end
    safe_join(badges, ' ')
  end

end