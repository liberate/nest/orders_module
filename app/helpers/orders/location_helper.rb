module Orders
  module LocationHelper
    protected
    #
    # displays a list of ISO3166 alpha3 country codes
    # or alpha2 region codes in a localized way.
    #
    def display_location_codes(codes, separator: ', ')
      if codes.nil?
        I18n.t("Worldwide")
      else
        codes.map {|code|
          case code
            when "AF" then I18n.t("Africa")
            when "AN" then I18n.t("Antarctica")
            when "AS" then I18n.t("Asia")
            when "EU" then I18n.t("Europe")
            when "NA" then I18n.t("North America")
            when "OC" then I18n.t("Oceania")
            when "SA" then I18n.t("South America")
            else ISO3166::Country.find_country_by_alpha3(code)&.local_name
          end
        }.join(', ')
      end
    end

    #
    # options for places we might or might not ship to.
    #
    def ship_to_options
      countries = [
        ["United States", "USA"],
        ["Puerto Rico", "PRI"],
        ["Canada", "CAN"]
      ]
      continents = [
        [I18n.t("Africa"),        "AF"],
        [I18n.t("Antarctica"),    "AN"],
        [I18n.t("Asia"),          "AS"],
        [I18n.t("Europe"),        "EU"],
        [I18n.t("North America"), "NA"],
        [I18n.t("Oceania"),       "OC"],
        [I18n.t("South America"), "SA"]
      ]
      countries + continents
    end
  end
end