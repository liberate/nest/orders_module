module Orders
  class DetailsController < ApplicationController
    optional_authentication
    optional_authorization
    layout 'center'
    def show
      @product_type = ProductType.find(params[:id])
    end
  end
end
