module Orders
  class OrdersController < UserApp::ApplicationController
    optional_authentication
    optional_authorization
    before_action :check_permission

    helper Orders::Engine.helpers

    def index
      @orders = current_user.member.orders.order(created_at: :desc).page(params[:page])
    end

    def show
    end

    private

    # allow anonymous if using the encrypted code
    def check_permission
      if params[:id]
        @order = Order.find_by_encrypted_code(params[:id])
        if @order
          permit!
        else
          @order = Order.find_by_code(params[:id]) || Order.find(params[:id])
          permit! { @order.member&.user == current_user }
        end
      end
    end

  end
end
