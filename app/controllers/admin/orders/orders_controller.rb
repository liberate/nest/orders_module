class Admin::Orders::OrdersController < AdminApp::ApplicationController
  helper Orders::Engine.helpers

  before_action :fetch_order, only: [:show, :edit, :update, :destroy]

  def index
    @orders = Order.order(created_at: :desc).includes(:member).page(params[:page])
    if params[:view]
      @orders = @orders.where(state: params[:view])
    end
  end

  def show
  end

  def edit

  end

  def update

  end

  def destroy
    if @order.transactions.empty?
      @order.destroy
      redirect_to(params[:return_url] || orders_path)
    end
  end

  private

  def fetch_order
    @order = Order.from_param(params[:id])
  end
end