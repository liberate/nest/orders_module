module Admin
  module Orders
    class ProductTypesController < AdminApp::ApplicationController
      helper Orders::Engine.helpers
      before_action :get_product_type, except: [:create, :index]

      def index
        @product_types = ProductType.order(category: :asc)
        if params[:view].nil?
          @product_types = @product_types.where(is_active: true)
        elsif params[:view] == 'inactive'
          @product_types = @product_types.where(is_active: false)
        end
      end

      def show
      end

      def update
        @product_type.update(product_type_params)
        if @product_type.valid?
          redirect_to admin_orders.product_type_url(@product_type, view: params[:view]), notice: "Product type was successfully updated."
        else
          render :show, status: :unprocessable_entity
        end
      end

      def create
        redirect_to ProductType.create!
      end

      private

      def product_type_params
        params.require(:product_type).permit(
          :type, :code, :title, :subtitle, :description, :category, :image, :region,
          :is_taxable, :quantity_type, :price, :value, :currency,
          :is_service, :is_active, :is_visible, :inventory, :is_shipping_required,
          :is_assignment_required, :distance_unit, :height, :width, :depth,
          :mass_unit, :weight, :front_matter, :end_matter, :specification,
          :shippable_to => []
        )
      end

      def get_product_type
        @product_type = ProductType.find params[:id]
      end
    end
  end
end