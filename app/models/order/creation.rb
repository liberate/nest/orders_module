module Order::Creation
  extend ActiveSupport::Concern

  included do
    attr_accessor :order_lines_changed
  end

  def add_line(item_definition_hash)
    create_order_lines(item_definition_hash).map do |ol|
      order_lines.append(ol)
      if ol.persisted?
        self.order_lines_changed = true
      else
        self.errors.add(:base, "Order line items not saved")
      end
      ol
    end
  end

  def add_lines(line_definitions)
    if line_definitions
      if line_definitions.is_a?(Hash)
        line_definitions = line_definitions.values # ignore keys
      end
      raise ArgumentError unless line_definitions.is_a?(Array)
      line_definitions&.each do |line|
        add_line(line)
      end
    end
  end

  class_methods do
    #
    # Central entrypoint for all order creation. No order
    # should be created except for with this method.
    #
    def creator(**args)
      args.stringify_keys!
      order = nil
      Order.transaction do
        lines = args.delete('lines')
        order = Order.create(**args)
        if order.persisted?
          order.add_lines(lines)
          yield order if block_given?

          order.update_totals! if order.order_lines_changed
          #order.send(:publish, :order_created, order)
        end
      end
      return order
    end
  end

  private

  #
  # A single line definition might create multiple order lines
  #
  def create_order_lines(line_definition)
    return [OrderLine.creator('order' => self, **line_definition)].flatten
  end

end
