class Shipment < ApplicationRecord
  has_many :orders, dependent: :nullify
  has_many :shipping_labels, through: :orders

  validate :validate_is_shipped

  before_destroy :reset_all_orders, prepend: true
  around_save :update_orders
  before_save :update_shipped_at

  # this is a flag used to determine if each membership in a shipment
  # should be auto-extended when a shipment ships.
  attr_accessor :extend_enrollment

  MAX_ORDERS = 50

  def self.add(order)
    shipment = find_open
    shipment.add(order)
    return shipment
  end

  def self.find_open
    # limit ourselves to shipments with fewer than 50 orders
    shipment = Shipment.joins(:orders).
      where(is_shipped: false, is_finished: false).
      group('shipments.id').
      having('count(orders.id) < ?', MAX_ORDERS).
      order('created_at DESC').take
    shipment ||= Shipment.create!(is_shipped: false, is_finished: false)
    return shipment
  end

  def add(order)
    if is_finished?
      raise ErrorMessage, 'This shipment is already finished'
    end
    order.shipment = self
    order.is_fulfilled = false
    order.save
  end

  def reset_all_orders
    unless self.is_shipped?
      self.orders.each do |order|
        order.update!(is_fulfilled: false)
      end
    end
  end

  def remove(order)
    if is_shipped?
      raise ErrorMessage, 'This shipment is already shipped'
    end
    orders.delete(order)
    order.update!(is_fulfilled: false)
  end

  #
  # returns a list of orders, grouped by product_type
  #
  # for example:
  #
  # {
  #    "None": [<order>],
  #    "Mifi M2000": [<order>, <order]
  # }
  #
  def orders_by_product_type(*includes)
    hsh = Hash.new {|h,k| h[k] = []}
    ords = includes ? ords = orders.includes(*includes) : orders
    ords.each do |order|
      ids = order.product_type_ids
      if ids.empty?
        hsh["None"] << order
      elsif ids.size > 1
        hsh["Multiple"] << order
      else
        label = ProductType.find_by_id(ids.first)&.title || "Unknown"
        hsh[label] << order
      end
    end
    hsh
  end

  #
  # returns a list of orders, grouped by shipping provider
  #
  # for example:
  #
  # {
  #    "None": [<order>],
  #    "USPS": [<order>, <order]
  # }
  #
  def orders_by_shipping_provider(*includes)
    hsh = Hash.new {|h,k| h[k] = []}
    ords = includes ? ords = orders.includes(*includes) : orders
    ords.each do |order|
      provider = order.shipping_label&.provider || "None"
      hsh[provider] << order
    end
    hsh
  end

  #
  # pays for all the labels in this shipment, in the background
  #
  # signer is the name of the person validating customs information (current user email)
  #
  def pay_for_labels(signer)
    Shipment.transaction do
      orders_with_unpaid_shipping_labels.each do |order|
        order.create_shipping_label if order.shipping_label.nil?
        order.shipping_label.async_get_rates_and_pay(signer)
      end
    end
  end

  def refund_labels
    Shipment.transaction do
      shipping_labels.where(is_paid: true).each do |label|
        label.async_refund
      end
    end
  end

  def label_jobs
    @label_jobs ||= DelayedJob.where(job_object_type: 'ShippingLabel', job_object_id: shipping_labels.ids)
  end

  #
  # returns a set of orders for this shipment that either have
  # shipping labels that are unpaid, or have no shipping label at all.
  #
  def orders_with_unpaid_shipping_labels
    orders.joins(
      "LEFT OUTER JOIN shipping_labels " +
      "ON shipping_labels.order_id = orders.id " +
      "AND shipping_labels.is_paid = TRUE"
    ).where(
      "shipping_labels.id IS NULL"
    )
  end

  protected

  def update_orders
    yield
    orders.each do |order|
      order.is_fulfilled = self.is_shipped?
      order.save
    end
  end

  def update_shipped_at
    if is_shipped_changed?
      self.shipped_at = Time.now.utc
    end
  end

  def validate_is_shipped
    if is_shipped_changed? && is_shipped == true
      if orders.unassigned.any?
        self.is_shipped = false
        self.errors.add(:base, :missing_assignments, "This shipment still has orders that are missing product assignments")
      end
    end
  end

end
