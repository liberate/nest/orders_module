#
# An Order record represents a single invoice.
#
class Order < ApplicationRecord
  #include Wisper::Publisher
  include Order::Creation

  has_code length: 6, type: :alphanum

  store :data, coder: JSON
  enum :state, [:new, :paid, :pending, :error, :canceled, :refunded], prefix: true # payment state

  belongs_to :member, optional: true
  belongs_to :shipment, optional: true
  has_many :order_lines, -> { includes(:product_type) }, inverse_of: 'order', dependent: :destroy
  has_one :shipping_label, dependent: :destroy

  has_many :products, through: :order_lines
  has_many :product_types, through: :order_lines

  has_many :transactions, dependent: :nullify

  monetize :amount_cents,   with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :value_cents,    with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :tax_cents,      with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :total_cents,    with_model_currency: :currency, numericality: true, allow_nil: true

  def shippable_to
    product_types.map(&:shippable_to).compact.reduce(&:intersection)
  end

  def is_paid?
    self.state_paid?
  end

  def update_totals!
    self.is_shipping_required = false
    self.is_assignment_required = false
    self.amount_cents = 0
    self.value_cents  = 0
    self.tax_cents    = 0
    missing_assignments = []

    order_lines.each do |ol|
      self.amount_cents += ol.amount_cents || 0
      self.value_cents  += ol.value_cents || 0
      self.tax_cents    += ol.tax_cents || 0
      self.is_shipping_required   ||= ol.is_shipping_required?
      self.is_assignment_required ||= ol.is_assignment_required?
      if ol.is_assignment_required? && !ol.has_assignment?
        missing_assignments << ol
      end
    end

    self.total_cents = self.tax_cents + self.amount_cents

    if self.is_assignment_required? && missing_assignments.empty?
      self.has_assignment = true
    end

    self.save!
  end

  def finalize_payment(customer)
    raise ArgumentError if !persisted? || total_cents.nil?
    payment = Transaction::CardPayment.attempt_payment(
      amount_cents: total_cents, currency: currency,
      customer: customer, member: member, order: self,
      record_matching_debit: true
    )
    if payment.state_success?
      self.update(state: "paid")
    else
      self.update(state: "error")
    end
  end

end