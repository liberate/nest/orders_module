class OrderLine < ApplicationRecord
  # associations
  belongs_to :order
  belongs_to :product_type, optional: true
  belongs_to :product,      optional: true

  # types
  store :data, coder: JSON
  enum :quantity_type, {once: 0, minutely: 1, hourly: 2, daily: 3, monthly: 4, quarterly: 5, yearly: 6}, prefix: :quantity
  monetize :unit_amount_cents, with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :unit_value_cents,  with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :amount_cents,      with_model_currency: :currency, numericality: true, allow_nil: false
  monetize :value_cents,       with_model_currency: :currency, numericality: true, allow_nil: false

  # validations
  validates :created_at, date: {allow_blank: true}
  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :title, presence: true

  # delegations & aliases
  delegate :is_shipping_required?, to: :product_type, allow_nil: true
  delegate :is_assignment_required?, to: :product_type, allow_nil: true

  # can overridden by subclasses
  def has_assignment?
    !self.product_id.nil?
  end

  #
  # The creator() method allows subclasses more freedom to add custom logic than if
  # they simply overrode 'create()'
  #
  def self.creator(**args)
    args.stringify_keys!
    if self != OrderLine
      return create(**args)
    else
      creator_class = get_creator_class(args)
      if creator_class.nil?
        return create(**args)
      else
        return creator_class.creator(**args)
      end
    end
  end

  # can overridden by subclasses
  #def needs_assignment?
  #  false
  #end

  #def has_assignment?
  #  false
  #end

  # called when the order has been paid for.
  # to be overridden by subclasses
  def finalize!
  end

  # called when the order has shipped.
  # to be overridden by subclasses
  def fulfilled!(options={})
  end

  private

  ##
  ## CALLBACKS
  ##

  #
  # product_types can have their values change over time, but orders are fixed,
  # so, we need to copy anything that might change in the product_type into the order_item
  #
  before_validation :copy_initial_values, on: :create
  def copy_initial_values
    if product_type
      self.title         ||= product_type.title
      self.subtitle      ||= product_type.subtitle
      self.quantity_type ||= product_type.quantity_type
      self.unit_amount   ||= product_type.price
      self.unit_value    ||= product_type.value
      self.currency      ||= product_type.currency
      self.is_taxable    ||= product_type.is_taxable
    end
  end

  before_save :update_amount
  def update_amount
    if self.unit_amount
      self.amount = self.quantity * self.unit_amount
    end
    if self.unit_value
      self.value = self.quantity * self.unit_value
    end
  end

  def self.get_creator_class(args)
    if args['type']
      creator_type = args.delete('type')
    elsif args['product_type']
      product_type = args['product_type']
      creator_type = product_type.order_line_type
    elsif args['product_type_id']
      product_type_id = args.delete('product_type_id')
      product_type = ProductType.find(product_type_id)
      args['product_type'] = product_type
      creator_type = product_type.order_line_type
    end

    return nil unless creator_type

    if creator_type.include?('::')
      return creator_type.to_s.constantize
    else
      return "OrderLine::#{creator_type.classify}".constantize
    end
  end
end