require 'open-uri'

#
# See https://goshippo.com/docs/reference
#
# Fields:
#
#   is_valid         -- true if the shipping record was created successfully.
#   is_paid          -- true if the label was created successfully.
#   has_error        -- false if this label is all good.
#   provider         -- string value of the shipping provider e.g. "USPS"
#   amount           -- what we paid for this label
#   resource_id      -- the shippo API id of the 'transaction' that purchased the label
#   tracking_id      -- the carrier specific tracking code
#   rate_id          -- ID of the rate we chose (or the cheapest rate if we haven't purchased yet)
#   shipment         -- JSON of the Shippo API shipment object (not a petal Shipment)
#   purchase         -- JSON of the Shippo API transaction object
#
# Supported values for label_file_type:
#
#   "PNG" "PNG_2.3x7.5" "PDF" "PDF_SINGLE_8X11" "PDF_W_PSLIP_8x11" "PDF_2.3x7.5"
#   "PDF_4x6" "PDF_4x8" "PDF_A4" "PDF_A6" "ZPLII"
#
#   Each potential carrier has different label_file_type that are supported. See https://goshippo.com/docs/carriers/
#
#   We use PNG, so it is easy to compose them into a PDF of our choosing.
#   However, PNG look bad when printed large, and they take up more storage space.
#
class ShippingLabel < ApplicationRecord
  belongs_to :order
  has_one :member, through: :order
  has_many :delayed_jobs, as: :job_object, dependent: :destroy

  has_one_attached :image

  store :shipment, coder: JSON, accessors: [:parcels, :address_from, :address_to, :rates]
  store :purchase, coder: JSON, accessors: [:label_url, :tracking_url_provider]

  monetize :amount_cents, with_model_currency: :currency,
           numericality: true, allow_nil: true

  ##
  ## ACCESSORS
  ##

  def has_rates?
    !shipment.empty?
  end

  def has_image?
    image.attached? || label_url.present?
  end

  def get_image
    if image.attached?
      image
    elsif self.purchase
      attach_label_image(self.purchase)
      image
    else
      nil
    end
  end

  # returns the full path on disk of the file if it is stored
  # locally
  def image_path
    ActiveStorage::Blob.service.path_for(order.shipping_label.image.key)
  end

  def ready_to_pay?
    has_rates? && !is_paid? && !rate_id.nil?
  end

  def messages
    msg = []
    msg.concat(shipment.dig(:messages)) if shipment.dig(:messages)
    msg.concat(purchase.dig(:messages)) if purchase.dig(:messages)
    return msg
  end

  def error_messages
    messages.map {|m| m["text"] if is_error_message?(m) }.compact.join('. ')
  end

  # the shipment data is only valid for 24 hours
  def shipment_expired?
    if shipment[:shipment_date]
      Time.parse(shipment[:shipment_date]) < 24.hours.ago
    else
      true
    end
  end

  def tracking_url
    if tracking_url_provider && Rails.env.production?
      tracking_url_provider.gsub(/^http:/, 'https:')
    else
      tracking_url_provider
    end
  end

  #
  # should this message be shown as a fatal error?
  #
  def is_error_message?(msg)
    if msg["type"] == "warning"
      false
    elsif msg["type"] == "fatal"
      true
    else
      msg[:code] =~ /(error|SubmissionDateTooOld)/ ||
      msg[:text] =~ /(must|invalid|warning|not responding|hard:)/i
    end
  end

  #
  # should this message be shown as a warning?
  #
  def is_warning_message?(msg)
    msg["type"] == "warning"
  end

  ##
  ## API ACTIONS
  ##

  #
  # Gets a list of all rates for this parcel and addresses.
  #
  # For international shipments, we need the name of the 'signer'
  # who is certifying the contents of the shipment. It should be
  # the name of the current user.
  #
  def get_rates(customs_signer: nil)
    clear_messages
    self.shipment = Shippo::Shipment.create(
      address_from: get_address_from,
      address_to:   get_address_to,
      parcels:      get_dimensions,
      customs_declaration: create_customs_declaration(customs_signer),
      async: false
    )
    shipment["messages"].each do |message|
      if is_error_message?(message)
        if shipment["status"] == "SUCCESS"
          message["type"] = "warning"
        else
          message["type"] = "fatal"
        end
      end
    end
    update_from_shipment(self.shipment)
  rescue StandardError => exc
    add_error(exc, :shipment)
  ensure
    save
  end

  #
  # buys the label, charges the account for the shipping
  #
  def pay(rate_id=nil)
    ShippingLabel.transaction do
      begin
        clear_messages
        rate_id ||= self.rate_id
        self.purchase = Shippo::Transaction.create(
          rate: rate_id,
          label_file_type: "PNG",
          async: false
        )
        update_from_transaction(purchase)
        attach_label_image(purchase)
        delayed_jobs.failed.destroy_all
      rescue StandardError => exc
        add_error(exc)
      ensure
        save
      end
    end
  end

  #
  # if the label has not been used, we can take it back
  #
  def refund
    clear_messages
    return unless is_paid?
    refund = Shippo::Refund.create(transaction: purchase[:object_id], async: false)
    if refund.nil?
      add_error("Empty response. Maybe label doesn't exist?")
    elsif refund[:status] == "SUCCESS" || refund[:status] == "PENDING"
      self.purchase = {}
      self.is_paid = false
      self.provider = nil
      self.amount = 0
      self.resource_id = nil
      self.tracking_id = nil
      image.purge
    else
      add_error("Unable to complete refund (#{refund[:status]})")
    end
  rescue StandardError => exc
    add_error(exc)
  ensure
    self.save
  end

  #
  # Runs a delayed job in the background to get the rates and purchase label.
  #
  # If there is an error, we raise an exception so that the job will be tried again.
  #
  # signer is the name we send to shippo for who validated the customs information.
  #
  def async_get_rates_and_pay(signer)
    if !is_valid? || shipment_expired?
      get_rates(customs_signer: signer)
    end
    if !is_paid? && is_valid? && rate_id
      pay
    end
    if has_error?
      raise StandardError, error_messages
    end
  end
  handle_asynchronously :async_get_rates_and_pay, queue: 'shipping'


  def async_refund
    if is_paid?
      refund
    end
    if has_error?
      raise StandardError, error_messages
    end
  end
  handle_asynchronously :async_refund, queue: 'shipping'

  ##
  ## RATE GUESSING
  ##

  #
  # fallback rule
  #   choose the cheapest ups
  # po box rule
  #   choose cheapest usps if po box
  # international rule
  #   choose cheapest usps if international
  # faster rule
  #   choose next cheapest ups if it is 2 days faster and not more than $2 more.
  # envelope rule
  #   choose cheapest usps if cheapest ups is $5 more & weight < 1 pound.
  #

  #
  # Returns the best shipping rates for the parcel and address.
  # precondition: get_rates()
  #
  # This does not always choose the cheapest, if there is another rate
  # that is slightly more but much faster.
  #
  def get_best_rate(shipment)
    return nil unless shipment && shipment[:rates]
    unless Conf.shippo_carriers.is_a?(Array) &&
           Conf.shippo_preferred_international_carriers.is_a?(Array) &&
           Conf.shippo_preferred_domestic_carriers.is_a?(Array)
      raise ArgumentError, 'Setting shippo_carriers is misconfigured'
    end

    rates = {"all" => []}
    rates_by_cost = shipment[:rates].sort { |a,b| a[:amount].to_f <=> b[:amount].to_f }
    rates_by_cost.each do |rate|
      Conf.shippo_carriers.each do |carrier|
        rates[carrier] ||= []
        rates["all"] << rate
        rates[carrier] << rate if rate[:provider] == carrier
      end
    end

    rate = rates[Conf.shippo_preferred_domestic_carriers.first].first || rates["all"].first

    if rate.nil?
      add_error("No rates available for shipping providers #{carriers.join(', ')}. Try again later.", :shipment)
      rate = nil
    elsif rates["all"].count == 1
      rate ||= rates["all"].first
    elsif !is_domestic?(from: shipment[:address_from], to: shipment[:address_to])
      rate = rates[Conf.shippo_preferred_international_carriers.first].first
    elsif is_po_box?(shipment[:address_to]) && rates["USPS"]
      rate = rates["USPS"].first
    elsif small_and_cheap = small_and_cheap(rates, rate, shipment)
      rate = small_and_cheap
    elsif faster = two_days_faster(rates, rate)
      rate = faster
    end

    return rate&.[](:object_id)
  end

  private

  def add_error(msg, storage=:purchase)
    self.send(storage)[:messages] ||= []
    self.send(storage)[:messages] << {text: msg.to_s, code: 'error'}
    self.has_error = true
  end

  def clear_messages
    self.shipment[:messages] = []
    self.shipment[:purchase] = []
  end

  #
  # given a rate id, return the rate hash from the list of possible
  # rates for this parcel and address.
  #
  # precondition: get_rates()
  #
  def get_rate(rate_id)
    self.shipment[:rates]&.each do |rate|
      if rate[:object_id] == rate_id
        return rate
      end
    end
    return {}
  end

  #
  # Returns the from address for this label.
  #
  def get_address_from
    address = {
      name:    Conf.shipping_from_name,
      street1: Conf.shipping_from_street1,
      street2: Conf.shipping_from_street2,
      city:    Conf.shipping_from_city,
      state:   Conf.shipping_from_state,
      zip:     Conf.shipping_from_zip,
      country: Conf.shipping_from_country,
      phone:   Conf.shipping_telephone
    }
  end

  #
  # returns the to address for this label.
  # International shipments require a phone number.
  # For now, we use our own phone number.
  #
  def get_address_to
    raise ArgumentError, 'Order does not have a member' if member.nil?
    raise ArgumentError, 'Member does not have an address' if member.mailing_address.nil?
    address = member.mailing_address.to_shippo_address(recipient: member.name)
    if address[:country] == Conf.shipping_from_country
      return address
    else
      return address.merge(phone: Conf.shipping_telephone)
    end
  end

  #def preferred_carriers(shipment)
  #  if is_domestic?(from: shipment[:address_from], to: shipment[:address_to])
  #    if is_po_box?(shipment[:address_to])
  #      Conf.shippo_carriers - ["UPS"] # UPS doesn't ship to PO Boxes
  #    else
  #      Conf.shippo_preferred_domestic_carriers
  #    end
  #  else
  #    Conf.shippo_preferred_international_carriers
  #  end
  #end

  def is_po_box?(address)
    if address
      addy = address["street1"]&.strip
      return addy =~ /^p\s*o\s*box/i || addy =~ /^p\s*o\s*#?\d+/i
    else
      return false
    end
  end

  #
  # returns true if this label is for a domestic shipment.
  #
  # A shipment is domestic if the country code is the same. For the US
  # we only treat the 50 states plus DC as domestic (because that is how
  # carriers handle it). US territories, US Virgin Islands, and Puerto Rico
  # are all considered international (except for the purposes of customs declaration).
  #
  def is_domestic?(from:, to:)
    if from && to
      from_country = Address::country_alpha3(from[:country])
      to_country   = Address::country_alpha3(to[:country])
      if from_country == to_country
        if from_country == "USA" && to[:state] && !Address::US_STATES.include?(to[:state])
          return false
        else
          return true
        end
      else
        return false
      end
    else
      return true
    end
  end

  # if the second cheapest rate is 2 days faster, choose that one if it is under $2 more
  def two_days_faster(rates, default_rate)
    provider = default_rate[:provider]
    if rates[provider][1] &&
       default_rate[:estimated_days].to_i > rates[provider][1][:estimated_days].to_i + 2 &&
       default_rate[:amount].to_f         > rates[provider][1][:amount].to_f - 2
      return rates[provider][1]
    else
      return nil
    end
  end

  # choose cheapest usps if cheapest ups is $5 more & weight < 1 pound.
  def small_and_cheap(rates, default_rate, shipment)
    if shipment["parcels"].first["mass_unit"] == "lb" && shipment["parcels"].first["weight"].to_f <= 1
      if Conf.shippo_carriers.include?("UPS") && Conf.shippo_carriers.include?("USPS")
        usps_rate = rates["USPS"].first
        if default_rate[:amount].to_f > usps_rate[:amount].to_f + 3
          return usps_rate
        end
      end
    end
    return nil
  end

  #
  # Customs declarations are not required between US states and US territories,
  # and also not required between some EU countries. TODO: find out which EU countries.
  #
  def customs_declaration_required?(from:, to:)
    from_country = Address::country_alpha3(from[:country])
    to_country   = Address::country_alpha3(to[:country])
    if from_country == "USA" && to_country == "USA"
      false
    else
      !is_domestic?(from: from, to: to)
    end
  end

  #
  # returns a hash that can be used as a Shippo parcel dimensions specification.
  #
  # for example:
  #
  # {length: 8, width: 6, height: 4, distance_unit: "in", weight: 1, mass_unit: "lb"}
  #
  def get_dimensions
    parcel_dimensions = order.parcel
    raise StandardError, 'Could not guess the parcel dimensions' if parcel_dimensions.nil?
    return parcel_dimensions
  end

  #
  # generates a hash to send to shippo for customs declaration.
  # this is only used for international shipments.
  #
  def get_items_for_customs
    items = []
    order.order_items.each do |oi|
      if oi.is_shipping_required?
        # UPS gets mad if the value is too low.
        value = oi.item.price_or_value
        if value < 1
          value = 1
        end
        items << {
          description: oi.item.display_name,
          quantity: oi.quantity,
          net_weight: (oi.quantity * oi.item.weight),
          mass_unit: oi.item.mass_unit,
          value_amount: (oi.quantity * value),
          value_currency: oi.item.currency,
          origin_country: Conf.shipping_from_country
        }
      end
    end
    return items
  end

  #
  # For international orders, we need a customs declaration object.
  #
  def create_customs_declaration(signer)
    if !member&.mailing_address
      nil
    else
      to =   {country: member.mailing_address.country, state: member.mailing_address.state}
      from = {country: Conf.shipping_from_country,     state: Conf.shipping_from_state}
      if customs_declaration_required?(to: to, from: from)
        Shippo::CustomsDeclaration.create(
          contents_type: "GIFT",
          contents_explanation: "Membership Premiums",
          non_delivery_option: "RETURN",
          certify: true,
          certify_signer: signer,
          items: get_items_for_customs
        )
      else
        nil
      end
    end
  end

  #
  # Update various fields based on the Shipment object
  # returned from Shippo.
  #
  def update_from_shipment(shipment)
    if shipment[:status] == "SUCCESS"
      self.is_valid = true
      self.has_error = false
    else
      self.has_error = true
    end
    self.rate_id = get_best_rate(shipment)
    shipment[:messages].each do |message|
      if is_error_message?(message)
        self.is_valid = false
        self.has_error = true
      end
    end
  end

  #
  # Update various fields based on the Transaction object
  # returned from Shippo.
  #
  def update_from_transaction(transaction)
    rate = get_rate(transaction[:rate])
    self.is_paid     = transaction[:status] == "SUCCESS"
    self.rate_id     = transaction[:rate]
    self.resource_id = transaction[:object_id]
    self.tracking_id = transaction[:tracking_number]
    self.provider    = rate[:provider]
    self.amount      = rate[:amount]
    self.currency    = rate[:currency]
    if transaction[:status] != "SUCCESS"
      add_error("Unable to complete purchase")
    else
      self.has_error = false
    end
  end

  #
  # Downloads the image from Shippo and saves it to active_storage.
  #
  def attach_label_image(purchase)
    if purchase[:label_url].present?
      image.purge
      image.attach(io: URI.open(purchase[:label_url]), filename: 'label.png')
    end
  end

  #
  # Ensure that this shipping label cannot be destroyed if the label has not been refunded
  #
  #def ensure_refund
  #  has_error = false
  #  refund
  #  if has_error?
  #    throw :abort
  #  end
  #end

end
