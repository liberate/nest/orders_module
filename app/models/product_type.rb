class ProductType < ApplicationRecord
  # associations
  belongs_to :record, polymorphic: true, optional: true
  has_one_attached :image
  #has_many_attached :images
  has_many :products

  # types
  monetize :price_cents, with_model_currency: :currency, numericality: true, allow_nil: true
  monetize :value_cents,  with_model_currency: :currency, numericality: true, allow_nil: true

  store :data, coder: JSON
  store_accessor :data, :front_matter, :end_matter, :specification, :options, :inventory_note, :features

  serialize :shippable_to, coder: JSON

  alias_attribute :display_name, :title
  enum :quantity_type, {once: 0, minutely: 1, hourly: 2, daily: 3, monthly: 4, quarterly: 5, yearly: 6}, prefix: :quantity
  enum :inventory, {full: 0, delayed: 1, low: 2, out: 3}, prefix: true

  # validations

  ## can be overridden by subclass
  #def is_shipping_required?
  #  false
  #end

  # can be overridden by subclass
  #def is_assignment_required?
  #  false
  #end
  #
  private

  before_save :clean_shippable_to
  def clean_shippable_to
    shippable_to&.select!(&:present?)
    write_attribute(:shippable_to, nil) if shippable_to&.empty?
  end

end