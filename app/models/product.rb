class Product < ApplicationRecord
  # associations
  belongs_to :member
  belongs_to :product_type
  belongs_to :replacement_for, class_name: 'Product'
  has_many :order_lines
  has_many :orders, through: :order_lines
  has_many :service_periods

  # types
  store :data, coder: JSON
  enum :placement, [:new, :assigned, :lost, :broken, :returned, :loaner], prefix: true

  # validations

end